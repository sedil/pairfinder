package root;

import java.util.ArrayList;
import java.util.List;

/**
 * Die Klasse Spieler erzeugt ein Memory-Teilnehmer
 * @author Sebastian
 */
public class Spieler {
	
	private List<Karte> karten;
	private final byte SPIELER_ID;
	private byte punkte;
	private boolean amZug;
	
	/**
	 * Der Konstruktor erzeugt ein Spieler samt festem ID-Wert zur identifikation
	 * @param id der ID-Wert
	 */
	public Spieler(byte id) {
		SPIELER_ID = id;
		punkte = 0;
		amZug = false;
		karten = new ArrayList<Karte>();
	}
	
	/**
	 * gibt dem Spieler eine Karte, falls er zwei Karten richtig aufgedeckt hat.
	 * Dafuer wird diese Funktion zweimal aufgerufen
	 * @param karte Die Karte die der Spieler bekommt
	 */
	public void addKarte(Karte karte) {
		karten.add(karte);
	}
	
	/**
	 * gibt alle Karten des Spielers am Spielende zurueck
	 * @return alle gewonnenen Karten aus einer Partie Memory
	 */
	public List<Karte> getSpielerkarten(){
		return karten;
	}
	
	/** erhoeht den Punktezaehler des Spielers um eins */
	public void erhoehePunkt() {
		punkte++;
	}
	
	/** setzt alle Spielerdaten fuer eine neue Runde Memory zurueck, heisst
	 *  der Punktestand wird zu 0 und alle Karten werden geloescht */
	public void setzeSpielerZurueck() {
		punkte = 0;
		amZug = false;
		karten.clear();
	}
	
	/**
	 * gibt den ID-Wert des Spielers zurueck
	 * @return Spieler ID
	 */
	public byte getID() {
		return SPIELER_ID;
	}
	
	/**
	 * legt fest, ob der Spieler an der Reihe ist
	 * @param istDran true, der Spieler ist dran
	 */
	public void setAmZug(boolean istDran) {
		amZug = istDran;
	}
	
	/**
	 * gibt den Punktestand des Spielers zurueck
	 * @return die Punkte des Spielers
	 */
	public byte getPunkte() {
		return punkte;
	}
	
	/**
	 * gibt zurueck, on der Spieler am Zug ist
	 * @return true, Spieler ist dran, false, Spieler ist nicht dran
	 */
	public boolean getAmZug() {
		return amZug;
	}

}
