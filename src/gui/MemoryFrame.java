package gui;

import java.util.List;
import java.util.Optional;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import root.Kontroller;

/**
 * Diese Klasse stellt die grafische Benutzeroberflaeche zwischen den
 * Computer und den Benutzer dar. Der Benutzer kann per Mausklick Karten
 * aufdecken und sieht wer an der Reihe ist samt Punktestand
 * @author Sebastian
 */
public class MemoryFrame extends Stage {
	
	private final short RESOLUTION = 635;
	private final String GRUEN = "-fx-background-color :#00FF00";
	private final String STANDARD = "-fx-background-color :#FFFFFF";
	private MemoryView mv;
	private Kontroller control;
	private BorderPane layout;
	private Label[] spielername, spielerpunkte;
	
	/**
	 * Der Konstruktor erwartet eine Liste mit mindestens 41 Bildern, eine Anzahl der
	 * teilnehmenden Spieler, die Anzahl der Kartenpaare und die maximale Spielfeldgroesse
	 * die je nach Anzahl der Paare angepasst wird
	 * @param bilder Images als Bilder
	 * @param anzahlSpieler anzahl der Teilnehmer (2 bis 4)
	 * @param anzahlPaare anzahl der Kartenpaare (8 bis 40)
	 * @param max spielfeldgroesse als Quadrat
	 */
	public MemoryFrame(List<Image> bilder, byte anzahlSpieler, byte anzahlPaare, byte max) {
		control = new Kontroller(bilder, anzahlSpieler, anzahlPaare, max);
		initComponents(anzahlSpieler);
		initLayout();
		
		Scene inhalt = new Scene(layout, RESOLUTION, RESOLUTION);
		setScene(inhalt);
		sizeToScene();
		setResizable(false);
		setTitle("Memory");
		show();
	}
	
	/**
	 * initialisiert die Komponenten
	 * @param anzahlSpieler anzahl der Spieler
	 */
	private void initComponents(byte anzahlSpieler) {
		mv = new MemoryView(this);
		spielername = new Label[anzahlSpieler];
		spielerpunkte = new Label[anzahlSpieler];
		for(byte a = 0; a < anzahlSpieler; a++) {
			spielername[a] = new Label("Spieler "+(a+1));
			spielername[a].setMaxWidth(128);
			spielername[a].setPadding(new Insets(0,25,0,25));
			spielerpunkte[a] = new Label("0");
			spielerpunkte[a].setMaxWidth(64);
			spielerpunkte[a].setPadding(new Insets(0,25,0,25));
		}
		spielername[0].setStyle(GRUEN);
		spielerpunkte[0].setStyle(GRUEN);
	}
	
	/** initialisiert das Layout des Fensters */
	private void initLayout() {
		layout = new BorderPane();	
		HBox statusleiste = new HBox();
		statusleiste.setPadding(new Insets(10,10,10,10));
		for(byte a = 0; a < spielername.length; a++) {
			statusleiste.getChildren().addAll(spielername[a], spielerpunkte[a]);
		}
		
		ScrollPane scroll = new ScrollPane(mv);
		layout.setCenter(scroll);
		layout.setBottom(statusleiste);	
	}
	
	/** aufrufen, wenn eine neue Spielrunde beginnen soll */
	public void neuesSpiel() {
		if ( !control.spielStatus()) {
			Alert frage = new Alert(AlertType.CONFIRMATION);
			frage.setTitle("Ergebnisverkuendung");
			frage.setHeaderText("Neues Memory-Spiel");
			frage.setContentText(control.ergebnisverkuendung()+"\nSoll eine neue Runde Memory geladen werden?");
			
			Optional<ButtonType> auswahl = frage.showAndWait();
			if ( auswahl.get() == ButtonType.OK) {
				control.neueSpielrunde();
				for(Label l : spielerpunkte) {
					l.setText("0");
				}
			} else {
				System.exit(0);
			}
		}
	}
	
	/**
	 * zeigt an welcher Spieler an der Reihe ist. Die Label werden
	 * gruen gefaerbt
	 * @param index die Spieler ID
	 */
	public void setLabelAktiverSpieler(byte index) {
		for(byte idx = 0; idx < spielername.length; idx++) {
			if(idx == index) {
				spielername[idx].setStyle(GRUEN);
				spielerpunkte[idx].setStyle(GRUEN);
			} else {
				spielername[idx].setStyle(STANDARD);
				spielerpunkte[idx].setStyle(STANDARD);
			}
		}
	}

	/**
	 * aktualisiert den Spielerstatustext
	 * @param index spieler ID
	 * @param punkte punkte der Spieler. Richtig gefundene Paare geben je ein Punkt
	 */
	public void setLabelText(byte index, byte punkte) {
		spielerpunkte[index].setText(Byte.toString(punkte));
	}

	/**
	 * gibt die Kontroller-Referenz zurueck
	 * @return der Kontroller
	 */
	public Kontroller getKontroller() {
		return control;
	}

}
