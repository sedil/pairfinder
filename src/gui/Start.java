package gui;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

/**
 * Diese Klasse ist die Starterklasse dieser Anwendung
 * @author Sebastian
 */
public class Start extends Application {

	private Stage fenster;
	private TextField sp, ka;
	private Button ok, bild;
	private List<Image> bilder;
	
	@Override
	public void start(Stage stage) throws Exception {
		fenster = stage;
		bilder = new ArrayList<Image>();
		Label spieler = new Label("Anzahl der Spieler : ");
		Label karten = new Label("Anzahl der Paare : ");
		Label bilder = new Label("Verzeichnis fuer Bilder : ");
		sp = new TextField("2");
		ka = new TextField("8");
		
		ok = new Button("Starten");
		ok.setDisable(true);
		ok.setOnAction(new ButtonHandler());
		bild = new Button("Bilder laden");
		bild.setOnAction(new ButtonHandler());
		
		HBox oben = new HBox();
		oben.getChildren().addAll(spieler, sp);
		
		HBox mitte = new HBox();
		mitte.getChildren().addAll(karten, ka);
		
		HBox unten = new HBox();
		unten.getChildren().addAll(bilder, bild);
		
		HBox ende = new HBox();
		ende.getChildren().add(ok);
		
		VBox layout = new VBox();
		layout.getChildren().addAll(oben, mitte, unten, ende);
		
		Scene inhalt = new Scene(layout);
		fenster.setScene(inhalt);
		fenster.sizeToScene();
		fenster.setTitle("PairFinder");
		fenster.setResizable(false);
		fenster.show();
	}
	
	/** laedt Bilder aus Bilddatein ein, falls erwuenscht */
	private void ladeBilder() {
		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().add(new ExtensionFilter("Bild-Datei","*.jpg"));
		List<File> datei = fc.showOpenMultipleDialog(null);
		if ( datei != null && datei.size() >= 41) {
			for(File f : datei) {
				Image temp = new Image(f.toURI().toString());
				if ( temp.getWidth() == 128 && temp.getHeight() == 128) {
					bilder.add(new Image(f.toURI().toString()));
				} else {
					Alert hinweis = new Alert(AlertType.WARNING);
					hinweis.setHeaderText("Bildformatfehler");
					hinweis.setContentText("Mindestens ein Bild hat eine ungueltige Aufloesung.\nUnterstuetzte Aufloesung : 128 x 128");
					hinweis.showAndWait();
					bilder.clear();
					ok.setDisable(true);
					return;
				}
			}
			ok.setDisable(false);
			bild.setDisable(true);
		} else {
			Alert hinweis = new Alert(AlertType.WARNING);
			hinweis.setHeaderText("Zu wenig Bilder vorhanden");
			hinweis.setContentText("Fuer ein Spiel PairFinder werden mindestens 41 Bilder benoetigt.\nFormat : jpg\nAufloesung : 128 x 128");
			hinweis.showAndWait();
			bilder.clear();
			ok.setDisable(true);
		}
	}
	
	/** prueft die Benutzereingaben, bevor eine MemoryFrame-Instanz erzeugt und dieses
	 *  Fenster geschlossen wird
	 */
	private void starte() {
		try {
			byte asp = Byte.parseByte(sp.getText());
			byte apa = Byte.parseByte(ka.getText());
			
			if ((asp >= 2 && asp <= 4) && (apa >= 8 && apa <= 40)) {
				byte feldmax = (byte) Math.ceil(Math.sqrt(2*apa + 1));
				new MemoryFrame(bilder, asp, apa, feldmax);
				fenster.close();
			} else {
				Alert warnung = new Alert(AlertType.WARNING);
				warnung.setHeaderText("Ungueltige eingabe");
				warnung.setContentText("Die eingegebenen Werte sind ungueltig.");
				warnung.showAndWait();
				sp.setText("2");
				ka.setText("8");
			}
		} catch ( NumberFormatException nfe ) {
			Alert error = new Alert(AlertType.ERROR);
			error.setHeaderText("Fehlerhafte eingabe");
			error.setContentText("Es duerfen nur Zahlen eingegeben werden!");
			error.showAndWait();
			sp.setText("2");
			ka.setText("8");
		}
	}
	
	private class ButtonHandler implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent act) {
			if( act.getSource() == ok) {
				starte();
			} else if ( act.getSource() == bild) {
				ladeBilder();
			}
		}
	}
	
	public static void main (String[] args) {
		launch(args);
	}

}
