package root;

import javafx.scene.image.Image;

/**
 * Diese Klasse stellt eine Karte zur Verfuegung mit fester ID,
 * ein Bild und ob die Karte auf- oder zugedeckt ist
 * @author Sebastian
 */
public class Karte {
	
	private final byte ID;
	private boolean istAufgedeckt;
	private Image bild;
	
	/**
	 * Erzeugt eine Karte mit festem ID-Wert zur Identifikation einer Karte
	 * @param id id-Nummer
	 */
	public Karte(byte id) {
		this.ID = id;
		istAufgedeckt = false;
		bild = null;
	}
	
	/**
	 * gibt der Karte ein Bild als Image
	 * @param bild ein Bild
	 */
	public void setBild(Image bild) {
		this.bild = bild;
	}
	
	/**
	 * gibt das Kartenbild zurueck
	 * @return
	 */
	public Image getBild() {
		return bild;
	}

	/**
	 * legt fest, ob die Karte als auf- oder zugedeckt markiert wird
	 * @param aufdecken true, Karte wird aufgedeckt, false, Karte wird zugedeckt
	 */
	public void deckeKarteAuf(boolean aufdecken) {
		istAufgedeckt = aufdecken;
	}
	
	/**
	 * gibt zurueck, ob die Karte auf- oder zugedeckt ist
	 * @return true, Karte aufgedeckt, false Karte zugedeckt
	 */
	public boolean istAufgedeckt() {
		return istAufgedeckt;
	}
	
	/**
	 * gibt den ID-Wert der Karte zurueck, ausserdem gilt der ID-Wert als
	 * "Standardbild" falls keine Kartenbilder vorhanden sind.
	 * @return
	 */
	public byte getID() {
		return ID;
	}
	
	/**
	 * prueft zwei uebergebene Karten auf gleichheit
	 * @param eins Karte eins
	 * @param zwei Karte zwei
	 * @return true, Karten sidn identisch, false sonst
	 */
	public static boolean istGleich(Karte eins, Karte zwei) {
		if ( eins.getID() == zwei.getID()) {
			return true;
		}
		return false;
	}

}
