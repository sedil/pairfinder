package root;

import ausnahmen.FeldException;

/**
 * Dies ist eine Hilfsklasse die eine Feldposition oder auch Zelle repraesentiert
 * @author Sebastian
 */
public class Feld {
	
	private Karte karte;
	private byte posx, posy;
	
	/**
	 * Erzeugt ein Feld, zunaechst ohne Inhalt
	 * @param x x-Koordinate
	 * @param y y-Koordinate
	 */
	public Feld(byte x, byte y) {
		posx = x;
		posy = y;
	}
	
	/**
	 * gibt dem Feld eine Memory-Karte
	 * @param karte die Memory-Karte
	 */
	public void setKarteAufFeld(Karte karte) {
		this.karte = karte;
	}
	
	/** entfernt aus dem Feld die Memory-Karte */
	public void entferneKarteAusFeld() {
		karte = null;
	}
	
	/**
	 * gibt die Karte zurueck, die sich auf den Feld befindet
	 * @return die ausgewaehlte Memory-Karte
	 * @throws FeldException Keine Karte vorhanden
	 */
	public Karte getKarteVonFeld() throws FeldException {
		if ( karte != null) {
			return karte;
		} else {
			throw new FeldException("Keine Karte im Feld bei : ["+posx+"]["+posy+"]");
		}
	}

}
