package ausnahmen;

@SuppressWarnings("serial")
/**
 * Erzeugt eine FeldException wenn ein Fehler bei einem Feld auftritt,
 * z.B. keine Karte im Feld
 * @author Sebastian
 */
public class FeldException extends Exception {
	
	/**
	 * Konstruktor der den individuellen Fehlertext an den
	 * Superkonstruktur weiterleitet
	 * @param fehlertext fehlertext
	 */
	public FeldException(String fehlertext) {
		super(fehlertext);
	}
}
